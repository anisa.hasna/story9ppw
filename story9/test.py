from django.test import LiveServerTestCase, TestCase, Client
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from datetime import datetime

class unitTest(TestCase):
	def test_apakah_ada_login_page(self):
		c = Client()
		response = c.get("/")
		self.assertEqual(response.status_code, 200)

	def test_apakah_pake_template_basehtml(self):
		c = Client()
		response = c.get("/")
		self.assertTemplateUsed(response, 'base.html')
		self.assertTemplateUsed(response, 'home.html')

	def test_apakah_ada_gif(self):
		c = Client()
		response = c.get("/")
		content = response.content.decode('utf8')
		self.assertIn("<blockquote", content)

	def test_apakah_ada_button_masuk(self):
		c = Client()
		response = c.get('/')
		content = response.content.decode('utf8')
		self.assertIn("<a", content)

class functionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(functionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(functionalTest, self).tearDown()

	def test_apakah_tombol_masuk_dapat_diklik(self):
		selenium = self.selenium
		selenium.get(self.live_server_url)
		time.sleep(3)
		tmp_masuk = selenium.find_element_by_id('masuk')
		time.sleep(3)
		tmp_masuk.send_keys(Keys.RETURN)

	def test_form_login(self):
		selenium = self.selenium
		selenium.get('http://localhost:8000/accounts/login')
		time.sleep(3)
		tmp_username = selenium.find_element_by_id('id_username')
		tmp_pass = selenium.find_element_by_id('id_password')
		time.sleep(3)

		tmp_username.send_keys('guest123')
		time.sleep(3)
		tmp_pass.send_keys('guestuser')